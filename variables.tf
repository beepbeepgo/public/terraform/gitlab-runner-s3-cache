variable "aws_account" {
  type        = string
  description = "AWS Account to deploy to"
}

variable "eks_cluster_identity_oidc_issuer" {
  type        = string
  description = "OIDC Issuer for the EKS cluster"
}
