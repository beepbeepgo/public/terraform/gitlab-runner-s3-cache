output "gitlab_runner_s3cache_bucket_name" {
  description = "Name of the s3 cache bucket"
  value       = module.s3_runner_cache_bucket.bucket_id
}

output "gitlab_runner_s3cache_bucket_arn" {
  description = "Arn of the s3 cache bucket"
  value       = module.s3_runner_cache_bucket.bucket_arn
}

output "gitlab_runner_s3cache_bucket_policy" {
  description = "Name of the S3 bucket policy for gitlab runner cache"
  value       = aws_iam_role_policy.bucket_policy.name
}

output "gitlab_runner_s3cache_bucket_object_policy" {
  description = "Name of the S3 bucket object policy for gitlab runner cache"
  value       = aws_iam_role_policy.bucket_object_policy.name
}

output "gitlab_runner_s3cache_role_name" {
  description = "Name of the iam role for the s3 bucket for gitlab runner cache"
  value       = aws_iam_role.gitlab-runner-s3cache.name
}
