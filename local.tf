locals {
  lifecycle_configuration_rules = [{
    enabled = true
    id      = "${module.this.id}-lifecycle"

    abort_incomplete_multipart_upload_days = 1 # number

    filter_and = null
    expiration = {
      days = 14 # integer > 0
    }
    noncurrent_version_expiration = {
      newer_noncurrent_versions = 2 # integer > 0
      noncurrent_days           = 7 # integer >= 0
    }
    transition                    = []
    noncurrent_version_transition = []
  }]

  oidc_issuer = replace(var.eks_cluster_identity_oidc_issuer, "https://", "")
}
