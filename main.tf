module "s3_runner_cache_bucket" {
  source  = "cloudposse/s3-bucket/aws"
  version = "3.1.1"

  acl     = "private"
  enabled = true

  user_enabled       = false
  versioning_enabled = false

  lifecycle_configuration_rules = local.lifecycle_configuration_rules

  context = module.this.context
}

resource "aws_iam_role_policy" "bucket_policy" {
  name = "${module.this.id}-bucket-policy"
  role = aws_iam_role.gitlab-runner-s3cache.id
  policy = jsonencode({
    Statement = [
      {
        Action = [
          "s3:ListBucket",
          "s3:ListBucketVersions"
        ]
        Effect   = "Allow"
        Resource = module.s3_runner_cache_bucket.bucket_arn
      }
    ]
  })
}

resource "aws_iam_role_policy" "bucket_object_policy" {
  name = "${module.this.id}-bucket-object-policy"
  role = aws_iam_role.gitlab-runner-s3cache.id
  policy = jsonencode({
    Statement = [
      {
        Action = [
          "s3:PutObject",
          "s3:PutObjectAcl",
          "s3:GetObject",
          "s3:ListBucket",
          "s3:ListBucketMultipartUploads",
          "s3:GetBucketLocation",
          "s3:AbortMultipartUpload"
        ]
        Effect   = "Allow"
        Resource = "${module.s3_runner_cache_bucket.bucket_arn}/*"
      }
    ]
  })
}

resource "aws_iam_role" "gitlab-runner-s3cache" {
  name = "${module.this.id}-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = "arn:aws:iam::${var.aws_account}:oidc-provider/${local.oidc_issuer}"
        }
        Condition = {
          "StringEquals" = {
            "${local.oidc_issuer}:aud" = "sts.amazonaws.com"
          }
        }
      }
    ]
  })
}
