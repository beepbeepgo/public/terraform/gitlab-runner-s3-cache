output "gitlab_runner_s3cache_bucket_name" {
  description = "Name of the s3 cache bucket"
  value       = module.gitlab_runner_s3cache.gitlab_runner_s3cache_bucket_name
}

output "gitlab_runner_s3cache_bucket_arn" {
  description = "Arn of the s3 cache bucket"
  value       = module.gitlab_runner_s3cache.gitlab_runner_s3cache_bucket_arn
}

output "gitlab_runner_s3cache_bucket_policy" {
  description = "Name of the S3 bucket policy for gitlab runner cache"
  value       = module.gitlab_runner_s3cache.gitlab_runner_s3cache_bucket_policy
}

output "gitlab_runner_s3cache_bucket_object_policy" {
  description = "Name of the S3 bucket object policy for gitlab runner cache"
  value       = module.gitlab_runner_s3cache.gitlab_runner_s3cache_bucket_object_policy
}

output "gitlab_runner_s3cache_role_name" {
  description = "Name of the iam role for the s3 bucket for gitlab runner cache"
  value       = module.gitlab_runner_s3cache.gitlab_runner_s3cache_role_name
}
