module "gitlab_runner_s3cache" {
  source = "../.."

  namespace   = "beepbeepgo"
  environment = "ci"

  aws_account = "1234567890987"

  eks_cluster_identity_oidc_issuer = "https://aws-oidc.com/ABIGHASH"
}
